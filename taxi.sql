select * from taxi_cash_20150511
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss'; 


select travel_mil,trans_amount,deal_time,day_time,ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) t 
from taxi_cash_20150511 where ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)  between 0 and 50 order by  travel_mil desc

--运营时间频数分布
select ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) t ,count(*) 
from taxi_cash_20150512 where ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)>0 
group by ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) order by  t desc


--运营里程频数分布
select travel_mil,count(*)
between 0 and 50 group by   travel_mil order by   travel_mil  desc

--按车牌号统计每辆车每天总的运营里程、运营时间
select car_code  , sum(travel_mil) ,sum(ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) )  
from taxi_cash_20150512  where   ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)  between 0 and 50
and travel_mil >0 and travel_mil <80
group by car_code

select car_code,DEAL_TIME,DAY_TIME, "空驶时间" ,"载客时间","载客时间"/("空驶时间" +"载客时间")from (select id,car_code,DEAL_TIME,DAY_TIME,
ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by id), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) as "空驶时间",
ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss')) * 24 * 60) "载客时间",untravel_mil
from taxi_cash_20150511  order by car_code,id) where "空驶时间" between 0 and 70 and "载客时间">0;

select   count(*) 
from taxi_cash_20150513  where   ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)  <0 or
ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)>114

select   * from taxi_cash_20150512  where day_num =1

select deal_time,day_time，ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)  from taxi_cash_20150511
where  day_num =1
 ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60)  <0 
 
 
 
--运营时间频数分布，根据day_num修正
select 运营时间,count(*) from
(select deal_time ,day_time,day_num,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 运营时间
from taxi_cash_20150512)
where 运营时间>=0
group by 运营时间
order by  运营时间 

--查询5月日运营次数、 营运车辆数、日均运营次数
select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日运营次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均运营次数 from taxi_cash_201505
where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2015-05-11' ,'yyyy-mm-dd')and  to_date('2015-05-24' ,'yyyy-mm-dd')
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

--查询9月日运营次数、 营运车辆数、日均运营次数
select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日运营次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均运营次数
 from taxi_cash_201409
where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-05' ,'yyyy-mm-dd')and  to_date('2014-09-18' ,'yyyy-mm-dd')
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

--按车牌号统计每辆车每天总的运营里程、运营时间，day_num修正
select car_code  , sum(travel_mil)/10 ,sum(载客时间),count(*)  载客次数
from 
(select car_code,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20140921)
where 载客时间 >0 and 载客时间<=144
and travel_mil > 0 and travel_mil<= 500 ---单位0.1公里
group by car_code order  by sum(travel_mil) desc

select  * from taxi_cash_20150513 where car_code='BS5074'

--查询5月出租车每天每小时载客次数,加入144、500阈值
select substr( deal_time ,1,10 ),substr( deal_time ,12,2 ),count(*) from 
(select deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 运营时间
from taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-05' ,'yyyy-mm-dd')and  to_date('2014-09-18' ,'yyyy-mm-dd') )
where  (运营时间 >0 and 运营时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr( deal_time ,1,10 ),substr( deal_time ,12,2 )
order by substr( deal_time ,1,10 ),substr( deal_time ,12,2 )

--查询X月日总载客次数、 营运车辆数、日均载客次数，加入144、500阈值
select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日载客次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均载客次数
 from (select  car_code,deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-05' ,'yyyy-mm-dd')and  to_date('2014-09-18' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)


select count(*) from tocc2.taxi_cash_201501
commit

--按车牌号统计每辆车每天总的运营里程、运营时间，day_num修正
select car_code   , sum(travel_mil) 出租车载客里程 ,sum(载客时间)  出租车载客时间
from 
(select car_code,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20150920)
where 载客时间 >0 and 载客时间<=144
and travel_mil > 0 and travel_mil<= 500 ---单位0.1公里
group by car_code order  by sum(travel_mil)



--查询1月出租车每天每小时载客次数,加入144、500阈值
select substr( deal_time ,1,10 ),substr( deal_time ,12,2 ),count(*) from 
(select deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from tocc2.taxi_cash_201501 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2015-01-05' ,'yyyy-mm-dd')and  to_date('2015-01-18' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr( deal_time ,1,10 ),substr( deal_time ,12,2 )
order by substr( deal_time ,1,10 ),substr( deal_time ,12,2 )

--查询1月日总载客次数、 营运车辆数、日均载客次数，加入144、500阈值
select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日载客次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均载客次数
 from (select  car_code,deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-08' ,'yyyy-mm-dd')and  to_date('2015-01-18' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

select ID,trans_amount,deal_time,wait_time,travel_mil,untravel_mil,day_num,day_time,car_code from taxi_cash_20150511


select  "空驶时间"  from 
(select id 编号,car_code 车牌号, DEAL_TIME 下车时间,DAY_TIME 上车时间,
ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) as "空驶时间",
ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss')) * 24 * 60) "载客时间"
from taxi_cash_20150401 order by car_code,deal_time) where "空驶时间">0 and rownum<=10000

select count(travel_mil+untravel_mil),avg(decode(travel_mil+untravel_mil,0,0,travel_mil/(travel_mil+untravel_mil))) 
from taxi_cash_20150105 where to_char(deal_time,'yyyy-mm-dd')='2012-11-05'--to_char('20130911','yyyy-mm-dd') ;  

--运营时间频数分布，根据day_num修正
select  travel_mil/10 载客里程,count(*) 频数 from
(select deal_time ,day_time,day_num, travel_mil,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20150513)
where (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by   travel_mil/10
order by   travel_mil/10

--0513每辆车的载客次数
select  car_code,count(*) 频数 from
(select car_code,deal_time ,day_time,day_num, travel_mil,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20150907)
where (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by   car_code

--查询1月日总载客次数、 营运车辆数、日均载客次数，加入144、500阈值
select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日载客次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均载客次数
 from (select  car_code,deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_201505 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2015-05-11' ,'yyyy-mm-dd')and  to_date('2015-05-24' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)


select substr(to_char(DEAL_TIME),0,10) 日期 ,count(*) 日载客次数,count(distinct car_code) 营运车辆数,round (count(*)/count(distinct car_code),1) 日均载客次数
 from (select  car_code,deal_time ,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from tocc2.taxi_cash_201509 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2015-09-07' ,'yyyy-mm-dd')and  to_date('2015-09-20' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

select count(*) from taxi_cash_20150513 where car_code='BP5658'


--成功
select   to_date('2014-09-05 00:00:00','yyyy-mm-dd hh24:mi:ss') -(level-1)/24/4  ti from dual connect by level <=96  order by ti 

--按半小时统计、加入阈值
select  substr( to_char(t2.ti),11,8), t2.ti, count(*) 频数  from
(select car_code,deal_time ,day_time,day_num, travel_mil,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20150117)  t1,
(select   to_date('2015-01-18 00:00:00','yyyy-mm-dd hh24:mi:ss') -(level-1)/24/2  ti from dual connect by level <=48 order by ti ) t2
where (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
and  t1.deal_time < ti and  t1.deal_time >=  t2.ti-1/24/2  
group by t2.ti order by ti


select car_code,DEAL_TIME,DAY_TIME, "空驶时间" ,"载客时间","载客时间"/("空驶时间" +"载客时间")from (select id,car_code,DEAL_TIME,DAY_TIME,
ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) as "空驶时间",
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_20150515  order by car_code,DEAL_TIME ) where "空驶时间" >0 ;


select car_code,DEAL_TIME,DAY_TIME, "空驶时间" ,"载客时间","载客时间"/("空驶时间" +"载客时间")from (select id,car_code,DEAL_TIME,DAY_TIME,
ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) as "空驶时间",
ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss')) * 24 * 60) "载客时间"
from  taxi_cash_20150515  order by car_code,id) where "空驶时间"  >0 ;

select count(travel_mil+untravel_mil),sum(travel_mil),sum(untravel_mil),avg(decode(travel_mil+untravel_mil,0,0,travel_mil/(travel_mil+untravel_mil))) 
from taxi_cash_20150515    

--里程利用率
select   substr(to_char(DEAL_TIME),0,10),count(distinct car_code),sum(travel_mil/10)/count(distinct car_code)   , sum(untravel_mil/10)/count(distinct car_code) ,
sum(travel_mil)/(sum(travel_mil)+sum(untravel_mil))
 from (select  car_code,deal_time ,untravel_mil,travel_mil ,case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-08' ,'yyyy-mm-dd')and  to_date('2014-09-21' ,'yyyy-mm-dd') )
where  (载客时间 >0 and 载客时间<=144)
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

--时间利用率
select car_code,DEAL_TIME,DAY_TIME, "空驶时间" ,"载客时间","载客时间"/("空驶时间" +"载客时间")
from (select id,car_code,DEAL_TIME,DAY_TIME,
ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) as "空驶时间",
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from taxi_cash_20140908  order by car_code,deal_time) where "空驶时间"  and "载客时间">0 ;

SELECT  * FROM  taxi_cash_20140908   WHERE CAR_CODE='BT5954'
select caR_CODE,COUNT(*) from taxi_cash_20140908 GROUP BY CAR_CODE ORDER BY  CAR_CODE DESC

SELECT * FROM taxi_cash_20140908 WHERE  
--每天的空驶时间、载客时间
select substr(to_char(DEAL_TIME),0,10),sum(空驶时间) ,sum(载客时间)，count(*)
from (select id,car_code,DEAL_TIME,DAY_TIME,travel_mil,
case day_num when 0 then 
  ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) 
when 1 then  ceil((1+(To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60)
end 空驶时间,
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-08' ,'yyyy-mm-dd')and  to_date('2014-09-09' ,'yyyy-mm-dd')
 order by car_code,deal_time) 
where ( 空驶时间 >0 and 空驶时间<=90)  
and( 载客时间 >0 and 载客时间<=144)  
and (travel_mil > 0 and travel_mil<= 500)
group by substr(to_char(DEAL_TIME),0,10)  order by substr(to_char(DEAL_TIME),0,10)

select (1+(to_date('00:05:00','hh24:mi:ss')-to_date('23:55:00','hh24:mi:ss')))*24*60  from dual

select (substr(to_char(DEAL_TIME),0,10)),count(*),sum(decode(sign(空驶时间),-1,0,空驶时间))/count(distinct car_code) , sum(载客时间）/count(distinct car_code) 
from (select id,car_code,DEAL_TIME,DAY_TIME,travel_mil,
case day_num when 0 then 
  ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) 
when 1 then  ceil((1+(To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60)
end 空驶时间,
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_201409 where to_date(substr(to_char(DEAL_TIME),0,10)) between  to_date('2014-09-08' ,'yyyy-mm-dd')and  to_date('2014-09-09' ,'yyyy-mm-dd')
 order by car_code,deal_time) 
where ( 空驶时间<=90)  
and( 载客时间 >0 and 载客时间<=144)  
and (travel_mil > 0 and travel_mil<= 500)
group by (substr(to_char(DEAL_TIME),0,10)) order by (substr(to_char(DEAL_TIME),0,10))


select  count(*), ceil((decode(sign(空驶时间),-1,0,空驶时间)))
from (select id,car_code,DEAL_TIME,DAY_TIME,travel_mil,
case day_num when 0 then 
  ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) 
when 1 then  ceil((1+(To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60)
end 空驶时间,
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_20140917
 order by car_code,deal_time) 
where 空驶时间<120
and ( 载客时间 >0 and 载客时间<=144)  
and (travel_mil > 0 and travel_mil<= 500)
group by ceil((decode(sign(空驶时间),-1,0,空驶时间))) order by ceil((decode(sign(空驶时间),-1,0,空驶时间))) 



select   (sum(decode(sign(空驶时间),-1,0,空驶时间))/count(distinct car_code)), (sum(载客时间）/count(distinct car_code))
from (select id,car_code,DEAL_TIME,DAY_TIME,travel_mil,
case day_num when 0 then 
  ceil(((To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60) 
when 1 then  ceil((1+(To_date(day_time,'hh24-mi-ss')-To_date(to_char(lag(DEAL_TIME )over (order by rownum), 'hh24-mi-ss'),'hh24-mi-ss')) )*24*60)
end 空驶时间,
case day_num 
when 0 then ceil((To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
when 1 then ceil((1+To_date(to_char(deal_time , 'hh24-mi-ss'),'hh24-mi-ss') - To_date(day_time,'hh24-mi-ss') )*24* 60) 
end 载客时间
from  taxi_cash_20140917
 order by car_code,deal_time) 
where ( 空驶时间<=90)  
and( 载客时间 >0 and 载客时间<=144)  
and (travel_mil > 0 and travel_mil<= 500)

--速度
--速度

