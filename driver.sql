alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss'; 
select sysdate from dual
drop table driver
--建表driver
create table driver(
driver_id varchar2(70),
passenger_id varchar2(70),
birth_time date,
order_status varchar2(5),
city_name varchar2(10),
current_lng NUMBER(15,6),
current_lat NUMBER(15,6),
starting_lng NUMBER(15,6),
starting_lat NUMBER(15,6),
dest_lng NUMBER(15,6),
dest_lat NUMBER(15,6),
starting_address varchar2(200),
dest_address varchar2(200),
strive_time varchar2(40),--有00的情况
finish_time varchar2(40),--有00的情况
total_fee NUMBER(5,1),
distance NUMBER(5,1),
begin_charge_time varchar2(40) --有00的情况
)



select count(*) from driver where city_name!='北京市'
commit
--删除非北京的
delete from fastcar where city_name!='北京市'
select driver_id ,count(*) from driver group by driver_id

select count(*) from fastcar where order_status=1
--查询28天的订单量
select substr(to_char(birth_time),1,10) ,count(*) from driver_new 
where order_status=1
group by substr(to_char(birth_time),1,10) order by substr(to_char(birth_time),1,10)

select count(*) from driver where substr(to_char(birth_time),1,10) ='2015-01-05'
--查询司机数
select count(distinct driver_id) from fastcar


--每天每小时统计
select substr(to_char(begin_charge_time),1,10 ),substr(to_char(begin_charge_time),12,2 ),count(*)
  from driver where order_status=1  
 group by substr(to_char(begin_charge_time),1,10 ),substr(to_char(begin_charge_time),12,2 ) 
 order by substr(to_char(begin_charge_time),1,10 ), substr(to_char(begin_charge_time),12,2 )
 
--无效数据标记
update fastcar set order_status=0 where  strive_time ='0000-00-00 00:00:00' or finish_time ='0000-00-00 00:00:00' 
or begin_charge_time ='0000-00-00 00:00:00'
--无效数据标记
update driver set order_status=0 where total_fee=0 or distance=0
--每天都有多少个小时有数据
select substr(to_char(begin_charge_time),1,10 ) ,count(distinct substr(to_char(begin_charge_time),12,2 ))
 from driver_new
group by substr(to_char(begin_charge_time),1,10 ) order by substr(to_char(begin_charge_time),1,10 ) 

--计算司机日营运时间，日运营里程
select driver_id,sum(ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)) 日营运时间,sum(distance) 日运营里程
from driver 
where order_status=1 and substr(to_char(begin_charge_time),1,10 )='2015-05-14'group by driver_id 

select * from driver where driver_id='790CD015' and substr(to_char(begin_charge_time),1,10 )='2015-05-14'
--无效数据标记
update fastcar set order_status=0 where total_fee=0 or distance=0

select count(*) from driver where order_status=1


select driver_id,count(driver_id),sum(ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)) 日营运时间,sum(distance) 日运营里程
from driver 
where order_status=1 and substr(to_char(begin_charge_time),1,10 )='2015-05-14'
group by driver_id order by 日运营里程


select * from driver

select finish_time-begin_charge_time from driver



--专车单记录行程时间频数统计 
select ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) t ,count(*)
from driver 
where order_status=1 
group by ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)
order by  t desc

--专车单记录里程频数统计
select distance,count(*)
from driver 
where ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 0 and 50 and 
order_status=1 group by distance  order by  distance desc


--计算司机日载客时间，日载客里程 加入144、50阈值
select driver_id,sum(ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)) 日载客时间,sum(distance) 日载客里程,count(*) 载客次数 
from fastcar 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),1,10 )='2015-09-09'
group by driver_id 

select  substr(to_char(begin_charge_time),1,7 ), count(*) 
from driver_new group by substr(to_char(begin_charge_time),1,7 )


--查询28天的日总载客次数司机数日人均载客次数 加入144 50阈值
select substr(to_char(begin_charge_time),1,10)日期 ,count(*)日总载客次数,count(distinct driver_id) 司机数,
round(count(*)/count(distinct driver_id),1) 日车均载客次数 from fastcar
where order_status=1 
and (ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144) 
and (distance between 0 and  50)
group by substr(to_char(begin_charge_time),1,10)
order by substr(to_char(begin_charge_time),1,10)


--每天每小时统计加入144 50阈值  
select substr(to_char(begin_charge_time),1,10 ),substr(to_char(begin_charge_time),12,2 ),count(*)
from fastcar where order_status=1 
and (ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144) 
and (distance between 0 and  50) 
group by substr(to_char(begin_charge_time),1,10 ),substr(to_char(begin_charge_time),12,2 ) 
order by substr(to_char(begin_charge_time),1,10 ), substr(to_char(begin_charge_time),12,2 )

select * from driver where order_status=1 and substr(to_char(begin_charge_time),1,10 )='0000-00-00'


select driver_id,order_status ,starting_lng,starting_lat,dest_lng,
dest_lat,strive_time,finish_time, 
total_fee,distance,
begin_charge_time from driver


--专车单记录行程时间频数统计 
select ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) t ,count(*)
from fastcar 
where ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1 
group by ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)
order by  t desc

--专车单记录里程频数统计
select distance ,count(*)
from fastcar 
where  ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1 group by distance  order by  distance desc

delete from driver_new where substr(to_char(begin_charge_time),1,10 )='2015-09-21'
select count(*) from driver_new where substr(to_char(begin_charge_time),1,10 )='2015-09-21'
commit

--三个月的订单数
select count(*),substr(to_char(begin_charge_time),1,7 ) from driver_new where order_status=1
group by substr(to_char(begin_charge_time),1,7 ) 
--查询错误的成功订单
select count(*) from fastcar where order_status=1 and (strive_time ='0000-00-00 00:00:00' or finish_time ='0000-00-00 00:00:00' 
or begin_charge_time ='0000-00-00 00:00:00'or total_fee=0 or distance=0)

select 
select substr(to_char(work_time),1,7 ), count(distinct driver_id) from driver_new  
group by substr(to_char(work_time),1,7 ) order by substr(to_char(work_time),1,7 )

select substr(to_char(begin_charge_time),1,7 ),count(*) from driver_new where order_status=1 
group by substr(to_char(begin_charge_time),1,7 )order by substr(to_char(begin_charge_time),1,7 )

--半小时统计 成功
select  substr( to_char(t2.ti),11,8), t2.ti,count(*) 频数 from fastcar t1 ,
(select   to_date('2015-09-20 00:00:00','yyyy-mm-dd hh24:mi:ss') -(level-1)/24/2  ti 
from dual connect by level <=48  order by ti ) t2
where   to_date(t1.begin_charge_time,'yyyy-mm-dd hh24:mi:ss') < ti and 
to_date( t1.begin_charge_time,'yyyy-mm-dd hh24:mi:ss') >=  t2.ti-1/24/2  
and t1.order_status=1
and substr(to_char(t1.begin_charge_time),1,10 )='2015-09-19'
and ceil((to_date(t1.finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(t1.begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (t1.distance between 0 and  50)
group by t2.ti order by ti


select count(distinct driver_id) from driver_new
 where order_status=1 AND substr(to_char(begin_charge_time),1,7 )='2015-01' 
 or substr(to_char(begin_charge_time),1,7 )='2015-05'

select count(distinct driver_id) from driver_new where order_status=1
and substr(to_char(begin_charge_time),1,7 )='2015-09' 

--建表driver
create table fastcar(
driver_id varchar2(70),
work_time date,
passenger_id varchar2(70),
birth_time date,
order_status varchar2(5),
city_name varchar2(10),
current_lng NUMBER(15,6),
current_lat NUMBER(15,6),
starting_lng NUMBER(15,6),
starting_lat NUMBER(15,6),
dest_lng NUMBER(15,6),
dest_lat NUMBER(15,6),
starting_address varchar2(200),
dest_address varchar2(200),
strive_time varchar2(40),--有00的情况
finish_time varchar2(40),--有00的情况
total_fee NUMBER(5,1),
distance NUMBER(5,1),
begin_charge_time varchar2(40) --有00的情况
)

select * from driver_new
select count(*)
commit
select starting_lat,starting_lng from fastcar where substr(to_char(begin_charge_time),1,10 )='2015-09-17'

--计算司机每天日载客时间，日载客里程的平均值 加入144、50阈值
select substr(to_char(begin_charge_time),1,10 ),sum(ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60))/count(distinct driver_id) 日载客时间,sum(distance)/count(distinct driver_id) 日载客里程
from fastcar 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
group by substr(to_char(begin_charge_time),1,10 ) order by substr(to_char(begin_charge_time),1,10 )


--快车单记录行程时间频数统计 
select ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) t ,count(*)
from fastcar 
where ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1 and substr(to_char(begin_charge_time),1,10 ) 
not in ('2015-05-13','2015-05-14','2015-05-15','2015-05-15','2015-05-16','2015-05-17')
group by ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)
order by  t desc

--快车单记录里程频数统计
select distance ,count(*)
from fastcar 
where  ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1 and substr(to_char(begin_charge_time),1,10 ) 
not in ('2015-05-13','2015-05-14','2015-05-15','2015-05-15','2015-05-16','2015-05-17')
group by distance  order by  distance desc

select count(*) from fastcar where order_status=1 
and substr(to_char(begin_charge_time),1,10 ) 
not in ('2015-05-13','2015-05-14','2015-05-15','2015-05-15','2015-05-16','2015-05-17')
and substr(to_char(begin_charge_time),1,7 )='2015-09' 

--计算工作日早高峰速度平均值 加入144、50阈值
select substr(to_char(begin_charge_time),1,10 ),sum(ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60))/count(distinct driver_id) 日载客时间,sum(distance)/count(distinct driver_id) 日载客里程
from fastcar 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),1,10 )
group by substr(to_char(begin_charge_time),1,10 ) order by substr(to_char(begin_charge_time),1,10 )

select substr(to_char(begin_charge_time),12,8 )  from fastcar 
where substr(to_char(begin_charge_time),12,2 ) in (7,8)
and substr(to_char(begin_charge_time),1,10 )='2015-09-09'
order by  substr(to_char(begin_charge_time),12,8 ) 

select substr(to_char(begin_charge_time),12,8 ),
avg(distance/((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24)) from fastcar 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),12,2 ) in (7,8)
and substr(to_char(begin_charge_time),1,10 )='2015-09-09'
order by  substr(to_char(begin_charge_time),12,8 ) 

--计算快车工作日早、晚高峰速度平均值,加入144、50阈值
select substr(to_char(begin_charge_time),1,10 ) 日期,round (avg(distance/((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24)),1) 非工作日平峰 from fastcar 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),12,2 ) in (6,7,8,9,12,13,14,15,18,19,20,21)--(7,8)(17,18) (6,9,10,11,12,13,14,15,16,19,20,21)
and substr(to_char(begin_charge_time),1,10 )  in --工作日                          --(10,11) (16,17) (6,7,8,9,12,13,14,15,18,19,20,21)
('2015-05-16','2015-05-17','2015-05-23','2015-05-24',
'2015-09-12','2015-09-13','2015-09-19','2015-09-20')
group by  substr(to_char(begin_charge_time),1,10 )
order by  substr(to_char(begin_charge_time),1,10 )

--计算专车工作日早、晚高峰速度平均值,加入144、50阈值
select substr(to_char(begin_charge_time),1,10 ) 日期,round (avg(distance/((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24)),1) 工作日平峰 from driver_new 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),12,2 ) in (17,18)--工作日(7,8)(17,18) (6,9,10,11,12,13,14,15,16,19,20,21)
and substr(to_char(begin_charge_time),1,10 )  not in --非工作日 (10,11) (16,17) (6,7,8,9,12,13,14,15,18,19,20,21)
('2015-01-10','2015-01-11','2015-01-17','2015-01-18',
'2015-05-16','2015-05-17','2015-05-23','2015-05-24',
'2015-09-12','2015-09-13','2015-09-19','2015-09-20')
group by  substr(to_char(begin_charge_time),1,10 )
order by  substr(to_char(begin_charge_time),1,10 )

select starting_lat,starting_lng,dest_lng,dest_lat from fastcar where substr(to_char(begin_charge_time),1,10 )='2015-09-16' and 
substr(to_char(begin_charge_time),12,2 ) in (17,18)

select * from fastcar

--导出某天早、晚高峰的行驶速度
select begin_charge_time,round ((distance/((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24)),2)  from driver_new 
where order_status=1 
and ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) between 1 and 144 
and distance between 0 and 50
and substr(to_char(begin_charge_time),12,2 ) in (17,18)--工作日(7,8)(17,18) (6,9,10,11,12,13,14,15,16,19,20,21)
and substr(to_char(begin_charge_time),1,10 ) = --非工作日 (10,11) (16,17) (6,7,8,9,12,13,14,15,18,19,20,21)
'2015-09-16'

order by  substr(to_char(begin_charge_time),1,10 )

--计算等待时间
select ceil((to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(strive_time,'yyyy-mm-dd hh24:mi:ss'))*24*60) t ,count(*)
from driver_new
where ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1
and substr(to_char(begin_charge_time),12,2 ) in (17,18)
group by ceil((to_date(begin_charge_time ,'yyyy-mm-dd hh24:mi:ss')
-to_date(strive_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)
order by  t

select starting_lng,starting_lat  from  driver_new where substr(to_char(begin_charge_time),1,10 )
between '2015-09-07' and '2015-09-12'
and order_status=1 
and substr(to_char(begin_charge_time),12,2 ) in (17,18);

select starting_lng,starting_lat   from  driver_new where substr(to_char(begin_charge_time),1,10 )
between '2015-09-14' and '2015-09-18'
and order_status=1 
and substr(to_char(begin_charge_time),12,2 ) in (7,8) union all
select starting_lng,starting_lat   from  fastcar where substr(to_char(begin_charge_time),1,10 )
between '2015-09-14' and '2015-09-18'
and order_status=1 
and substr(to_char(begin_charge_time),12,2 ) in (7,8) union all
select starting_lng,starting_lat   from  fastcar where substr(to_char(begin_charge_time),1,10 )
between '2015-09-07' and '2015-09-11'
and order_status=1 
and substr(to_char(begin_charge_time),12,2 ) in (7,8) union all
select starting_lng,starting_lat   from  driver_new where substr(to_char(begin_charge_time),1,10 )
between '2015-09-07' and '2015-09-11'
and order_status=1 
and substr(to_char(begin_charge_time),12,2 ) in (7,8)

select ceil((to_date(strive_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(birth_time,'yyyy-mm-dd hh24:mi:ss'))*24*60*60) t 
from fastcar
where ceil((to_date(finish_time,'yyyy-mm-dd hh24:mi:ss')
-to_date(begin_charge_time,'yyyy-mm-dd hh24:mi:ss'))*24*60)between 1 and 144 
and  (distance between 0 and  50)
and order_status=1 --and substr(to_char(begin_charge_time),1,10 ) 
--='2015-09-11'
and substr(to_char(begin_charge_time),12,2 ) in (17,18)
order by  t desc
select * from fastcar

select birth_time  from fastcar order by birth_time asc
select substr(to_char(begin_charge_time),7,1 ), count(*) from fastcar where order_status=1 group by substr(to_char(begin_charge_time),7,1 )

select substr(to_char(begin_charge_time),7,1 ),快车司机数+专车司机数 from
 (select substr(to_char(begin_charge_time),7,1 ),count(distinct(driver_id))快车司机数 
 from fastcar where order_status=1 group by substr(to_char(begin_charge_time),7,1 )
union all
select substr(to_char(begin_charge_time),7,1 ),count(distinct(driver_id)) 专车司机数 
from driver_new where order_status=1 group by substr(to_char(begin_charge_time),7,1 ) ) 
group by substr(to_char(begin_charge_time),7,1 )

select count(distinct(driver_id)) from  fastcar where order_status=1
